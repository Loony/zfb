# Problems so far:

* Client warnings and errors
* Flying stuff handling and firing
* Bazooka and gun firing with "Use" button.
* Building selection menu is not convinient


## Building menu structure

* Civilian
    * Builder Shop
    * Quarters
    * Knight Shop
    * Boulder Factory
    * Saw Factory
    * Lantern Factory
    * Flag Factory
    * Kitchen
    * Trader
* Military
    * Land
        * Knight
        * Archer
        * Vehicle shop
        * Bison Nursery
        * Bombs factory
        * Machine gun factory
        * Bazooka factory
    * Naval
        * Fish
        * Boat
        * Warboat
    * Air
        * Glider
        * Bomber
        * Missile ship
        * Private baloon
* Economic
    * Storage
    * Wood refinery
    * Stone refinery
    * Gold refinery
    * Tree seeds
    * Plant
    * Trader

## Things to consider

* Sniper rifle
